import chalk from 'chalk';
import { Express } from 'express';
import figlet from 'figlet';
import _ from 'lodash';

export enum Severity {
  LOG = 'log',
  INFO = 'info',
  WARN = 'warn',
  ERROR = 'error'
}
export interface Log {
  readonly severity: Severity;
  readonly content: any;
}

export const log = (message: Log) => console[message.severity](message.content);
export const info = (content: any) => log({ severity: Severity.INFO, content });
export const warn = (content: any) => log({ severity: Severity.WARN, content });
export const error = (content: any) => log({ severity: Severity.ERROR, content });

export const logAppInfo = (app: Express) => info(`
${figlet.textSync(app.get('banner'))}
  Url: ${ app.get('port') ? chalk.blue.underline.bold(`http://localhost:${app.get('port')}`) : 'none' }
  Mode: ${ chalk.bold(app.get('env')) }
`);

const BANNER_STAR_LIMIT = 60;
const generateBannerStars = (length: number) => _.times(length > BANNER_STAR_LIMIT
  ? BANNER_STAR_LIMIT
  : length, () => '*').join('')

export const logObvious = (content: any) => info(`
${generateBannerStars(content.length)}
${content}
${generateBannerStars(content.length)}
`);
