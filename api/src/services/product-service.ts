import axios from 'axios';
import { JSDOM, VirtualConsole } from 'jsdom';
import { isNullOrUndefined } from 'util';
import { error, info, warn } from './logger-service';

export enum ProductType {
  SANDWICH = 'Broodjes',
  TRIANGLE = 'Triangles',
  SANDWINO = 'Sandwinos',
  WRAP = 'Wraps',
  BURGER = 'Burgers',
  HOT = 'Warm',
  COLD = 'Koud',
  DESSERT = 'Desserten',
  MEETING = 'Meeting'
}

export interface Product {
  readonly id: string;
  readonly name: string;
  readonly description: string;
  readonly price: number;
  readonly type: ProductType
}

const getDocumentFor = (html: string) => {
  const virtualConsole = new VirtualConsole();
  const { window: { document } } = new JSDOM(html, { virtualConsole });
  virtualConsole.on('dir', info);
  virtualConsole.on('info', info);
  virtualConsole.on('warn', warn);
  virtualConsole.on('error', error);
  return document;
}

const getIdFrom = (row: HTMLTableRowElement) => {
  const select = row.querySelector<HTMLSelectElement>('select');
  const id = isNullOrUndefined(select) ? '' : select.getAttribute('name');
  return isNullOrUndefined(id) ? '' : id;
}

const getNameFrom = (row: HTMLTableRowElement) => {
  const td = row.querySelectorAll<HTMLTableDataCellElement>('td')[1];
  return isNullOrUndefined(td) ? '' : td.innerHTML;
}

const getDescriptionFrom = (row: HTMLTableRowElement) => {
  const td = row.querySelectorAll<HTMLTableDataCellElement>('td')[2];
  return isNullOrUndefined(td) ? '' : td.innerHTML;
}

const getPriceFrom = (row: HTMLTableRowElement) => {
  const td = row.querySelectorAll<HTMLTableDataCellElement>('td')[3];
  return isNullOrUndefined(td) ? 0 : Number(td.innerHTML);
}

const typeToHeading = (type: ProductType): string => {
  return {
    [ProductType.SANDWICH.toString()]: 'Belegde broodjes',
    [ProductType.TRIANGLE.toString()]: 'Triangle',
    [ProductType.SANDWINO.toString()]: 'Sandwino\'s',
    [ProductType.WRAP.toString()]: 'Wrap',
    [ProductType.BURGER.toString()]: 'Burgers',
    [ProductType.HOT.toString()]: 'Warme gerechten',
    [ProductType.COLD.toString()]: 'Koude schotels',
    [ProductType.DESSERT.toString()]: 'Dessert'
  }[type]
};

const getRowsFor = (document: Document, type: ProductType): NodeListOf<HTMLTableRowElement> => Array
  .from(document.querySelectorAll('h4'))
  .filter(heading => heading.textContent === typeToHeading(type))
  .map(heading => (heading.parentElement as HTMLElement).querySelector<HTMLTableElement>('table') as HTMLTableElement)[0]
  .querySelectorAll<HTMLTableRowElement>('tr');


const getProductsFrom = (document: Document, type: ProductType): ReadonlyArray<Product> => Array
  .from(getRowsFor(document, type))
  .map(element => ({
    id: getIdFrom(element),
    name: getNameFrom(element),
    description: getDescriptionFrom(element),
    price: getPriceFrom(element),
    type
  }))
  .slice(1);

const mapHtmlProductsToProducts = (html: string): ReadonlyArray<Product> => {
  const document = getDocumentFor(html);
  return [
    ...getProductsFrom(document, ProductType.SANDWICH),
    ...getProductsFrom(document, ProductType.TRIANGLE),
    ...getProductsFrom(document, ProductType.SANDWINO),
    ...getProductsFrom(document, ProductType.WRAP),
    ...getProductsFrom(document, ProductType.BURGER),
    ...getProductsFrom(document, ProductType.HOT),
    ...getProductsFrom(document, ProductType.COLD),
    ...getProductsFrom(document, ProductType.DESSERT)
  ]
}

// TODO: make interceptor to always set sessionId instead of passing it manually with each call
export const getProducts = async (sessionId: string): Promise<ReadonlyArray<Product>> => axios
  .get(`${process.env.TAMBEUR_URL}/stap1.php`, {
    headers: { Cookie: `PHPSESSID=${sessionId}` }
  })
  .then(({ data }) => mapHtmlProductsToProducts(data));
