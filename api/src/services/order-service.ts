import axios from 'axios';
import memoize from 'lodash/memoize';
import querystring from 'querystring';
import { Product, ProductType } from './product-service';

export interface Order {
  readonly product: Product;
  readonly amount: number;
  readonly extra: string;
  readonly not: string;
  readonly reference: string;
}

export interface SandwichOrder extends Order {
  readonly bread: string;
  readonly club: boolean;
}

export interface BurgerOrder extends Order {
  readonly saus: string;
}

export interface HotOrder extends Order {
  readonly bread: string;
}

export interface ColdOrder extends Order {
  readonly large: boolean;
}

export type AnyOrder = Order | SandwichOrder | BurgerOrder | HotOrder | ColdOrder;

export interface OrderResponse {
  readonly success: boolean;
  readonly html: string;
}

const countFor = (type: ProductType, orders: ReadonlyArray<Order>) => orders
  .filter(orderToFilter => orderToFilter.product.type === type)
  .map(orderToMap => orderToMap.amount)
  .reduce((a1, a2) => a1 + a2, 0);

const mergeObjects = (o1: any, o2: any) => ({ ...o1, ...o2 });

const getAantallen = (orders: ReadonlyArray<Order>) => [
  ProductType.SANDWICH,
  ProductType.MEETING,
  ProductType.TRIANGLE,
  ProductType.SANDWINO,
  ProductType.WRAP,
  ProductType.BURGER,
  ProductType.HOT,
  ProductType.COLD,
  ProductType.DESSERT]
  .map((type, index) => ({ [`aantal${index + 1}`]: countFor(type, orders) }))
  .reduce((o1, o2) => ({ ...o1, ...o2 }), {});

const extractId = memoize((idWithAantal: string): RegExpExecArray | null => new RegExp(/^((.?)*)-aantal$/).exec(idWithAantal));

const throwError = (message: string) => { throw message; }

const idFrom = memoize((product: Product): string => extractId(product.id) !== null
  ? (extractId(product.id) as RegExpExecArray)[1]
  : throwError('Product ID could not be found'));

const mapSandwichOrderToTambeurOrder = ({ product, amount, bread, club, extra, not, reference }: SandwichOrder) => ({
  [idFrom(product)]: idFrom(product),
  [`${idFrom(product)}-aantalstap2`]: amount,
  [`${idFrom(product)}-soortbrood0`]: bread,
  [`${idFrom(product)}-club0`]: club ? 'ja' : 'nee',
  [`${idFrom(product)}-extra0`]: extra,
  [`${idFrom(product)}-niet0`]: not,
  [`${idFrom(product)}-aan0`]: reference
});

const mapBurgerOrderToTambeurOrder = ({ product, amount, saus, extra, not, reference }: BurgerOrder) => ({
  [idFrom(product)]: idFrom(product),
  [`${idFrom(product)}-aantalstap2`]: amount,
  [`${idFrom(product)}-saus0`]: saus,
  [`${idFrom(product)}-extra0`]: extra,
  [`${idFrom(product)}-niet0`]: not,
  [`${idFrom(product)}-aan0`]: reference
});

const mapHotOrderToTambeurOrder = ({ product, amount, bread, extra, not, reference }: HotOrder) => ({
  [idFrom(product)]: idFrom(product),
  [`${idFrom(product)}-aantalstap2`]: amount,
  [`${idFrom(product)}-brood0`]: bread,
  [`${idFrom(product)}-extra0`]: extra,
  [`${idFrom(product)}-niet0`]: not,
  [`${idFrom(product)}-aan0`]: reference
});

const mapColdOrderToTambeurOrder = ({ product, amount, large, extra, not, reference }: ColdOrder) => ({
  [idFrom(product)]: idFrom(product),
  [`${idFrom(product)}-aantalstap2`]: amount,
  [`${idFrom(product)}-groot0`]: large ? 'groot' : 'klein',
  [`${idFrom(product)}-extra0`]: extra,
  [`${idFrom(product)}-niet0`]: not,
  [`${idFrom(product)}-aan0`]: reference
});

const mapBasicOrderToTambeurOrder = ({ product, amount, extra, not, reference }: Order) => ({
  [idFrom(product)]: idFrom(product),
  [`${idFrom(product)}-aantalstap2`]: amount,
  [`${idFrom(product)}-extra0`]: extra,
  [`${idFrom(product)}-niet0`]: not,
  [`${idFrom(product)}-aan0`]: reference
});

const mappingFunctionFor = (type: ProductType) => (_: AnyOrder): Object => ({
  [ProductType.SANDWICH]: mapSandwichOrderToTambeurOrder,
  [ProductType.MEETING]: mapSandwichOrderToTambeurOrder, // TODO: fix this one
  [ProductType.TRIANGLE]: mapBasicOrderToTambeurOrder,
  [ProductType.SANDWINO]: mapBasicOrderToTambeurOrder,
  [ProductType.WRAP]: mapBasicOrderToTambeurOrder,
  [ProductType.BURGER]: mapBurgerOrderToTambeurOrder,
  [ProductType.HOT]: mapHotOrderToTambeurOrder,
  [ProductType.COLD]: mapColdOrderToTambeurOrder,
  [ProductType.DESSERT]: mapBasicOrderToTambeurOrder
}[type]);

const mapOrderToTambeurOrder = (order: AnyOrder) => mappingFunctionFor(order.product.type)(order) // tslint:disable-line

// TODO: test this function
const mapOrdersToTambeurOrders = (orders: ReadonlyArray<AnyOrder>) => querystring.stringify({
  bestellen: 'Bevestigen',
  ...getAantallen(orders),
  ...orders
    .map(mapOrderToTambeurOrder)
    .reduce(mergeObjects, {})
});

// TODO: make interceptor to always set sessionId instead of passing it manually with each call
export const order = async (sessionId: string, orders: ReadonlyArray<AnyOrder>): Promise<OrderResponse> => axios
  .post<string>(
    `${process.env.TAMBEUR_URL}/stap3.php`,
    mapOrdersToTambeurOrders(orders),
    { headers: { Cookie: `PHPSESSID=${sessionId}` } })
  .then(({ data }) => ({
    success: data.includes('Uw bestelling is geplaatst!'),
    html: data
  }));

export const fakeOrder = async (_: string, orders: ReadonlyArray<AnyOrder>): Promise<OrderResponse> => {
  console.log(orders);
  return Promise.resolve({
    success: true,
    html: 'Testing...'
  });
}
