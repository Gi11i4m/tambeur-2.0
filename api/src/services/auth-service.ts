import axios, { AxiosResponse } from 'axios';
import querystring from 'querystring';

interface Credentials {
  readonly username: string;
  readonly password: string;
}

interface Session {
  readonly sessionId: string;
}

const throwErrorIfIncorrectCredentials = (response : AxiosResponse<any>) => {
  if (response.data.includes('het aanmelden is mislukt')) { // tslint:disable-line
    throw new Error('Aanmelden mislukt');
  }
  return response;
}

const extractSessionId = (response: AxiosResponse<any>) => ({
  sessionId: response.headers['set-cookie'][0].match(/PHPSESSID=([^;]*)/)[1]
});

export const login = async (credentials: Credentials): Promise<Session> => axios
  .post(`${process.env.TAMBEUR_URL}/index.php`, querystring.stringify({
    gebruikersnaam: credentials.username,
    wachtwoord: credentials.password,
    aanmelden: 'Aanmelden'
  }))
  .then(throwErrorIfIncorrectCredentials)
  .then(extractSessionId);
