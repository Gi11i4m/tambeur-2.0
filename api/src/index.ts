import * as bodyParser from 'body-parser';
import express from 'express';
import http from 'http';
import { OK } from 'http-status-codes';
import { authApp } from './controllers/auth.controller';
import { orderApp } from './controllers/order.controller';
import { productApp } from './controllers/product.controller';
import { logAppInfo } from './services/logger-service';

require('dotenv').config(); // tslint:disable-line

const PORT = process.env.PORT;

const app = express();
const httpServer = new http.Server(app);
// const io = socketIo(httpServer);

app
  /* API */
  .use(bodyParser.json())
  .use('/health', (_, res) => res.send(OK))
  .use('/api/auth', authApp)
  .use('/api/products', productApp)
  .use('/api/orders', orderApp)
  /* APP */
  .use(express.static(`${__dirname}/app/`))
  .get('*', (_, res) => res.sendFile(`${__dirname}/app/index.html`))
  /* SERVER */
  .set('banner', 'Tambeur 2.0')
  .set('port', PORT);

httpServer.listen(PORT, () => logAppInfo(app));
