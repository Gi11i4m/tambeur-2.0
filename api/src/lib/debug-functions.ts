import { info } from '../services/logger-service';

export function logAndContinue<T>(value: T): T {
  info(value);
  return value;
}
