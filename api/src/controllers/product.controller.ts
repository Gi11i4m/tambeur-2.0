import express from 'express';
import { INTERNAL_SERVER_ERROR } from 'http-status-codes';
import { getProducts } from '../services/product-service';

export const productApp = express()
  .get('/', (req, res) => getProducts(req.header('SessionId') as string)
    .then(products => res.send(products))
    .catch(error => {
      console.error(error); // TODO: move this to interceptor / middleware
      res.status(INTERNAL_SERVER_ERROR).send(error);
    }));
