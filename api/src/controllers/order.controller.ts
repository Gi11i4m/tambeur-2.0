import express from 'express';
import { INTERNAL_SERVER_ERROR } from 'http-status-codes';
import { order } from '../services/order-service';

export const orderApp = express().post('/', (req, res) =>
  order(req.header('SessionId') as string, req.body)
    .then(result => res.send(result))
    .catch(error => res.status(INTERNAL_SERVER_ERROR).send(error))
);
