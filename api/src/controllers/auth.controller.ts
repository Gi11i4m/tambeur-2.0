import express from 'express';
import { login } from '../services/auth-service';
import { error } from '../services/logger-service';

export const authApp = express()
  .post('/', (req, res) => login(req.body)
    .then(response => res.send(response))
    .catch(error));
