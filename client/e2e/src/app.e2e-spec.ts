import { AppPage } from './app.po';

describe('App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should run at least one test', () => {
    page.navigateTo();
    expect(true).toBeTruthy();
  });
});
