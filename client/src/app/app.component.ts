import { Component } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { of } from 'rxjs';
import { filter, mergeMap } from 'rxjs/operators';
import { isNull } from 'util';
import { CartComponent } from './cart/cart.component';
import { AuthService } from './core/auth.service';
import { CartService } from './core/cart.service';
import { OrderResponse, OrderService } from './core/order.service';

@Component({
  selector: 'tb-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private authService: AuthService,
    private cartService: CartService,
    private orderService: OrderService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar) {}

  get isAuthenticated() {
    return this.authService.isAuthenticated;
  }

  get numberOfProductsInCart() {
    return this.cartService.numberOfProducts;
  }

  openCart() {
    this.dialog
      .open(CartComponent, { width: '50rem' })
      .afterClosed()
      .pipe(
        mergeMap(orders => !!orders ? this.orderService.order(orders) : of(null)),
        // mergeMap(orders => !!orders ? of({ success: false }) : of(null)),
        filter(orders => !isNull(orders)))
      .subscribe((result: OrderResponse) => result.success ? this.success() : this.error());
  }

  private success() {
    this.cartService.clear();
    this.snackBar.open('Your order has been placed!', 'dismiss', { panelClass: 'success-snackbar' });
  }

  private error() {
    this.snackBar.open('Something went wrong...', 'dismiss', { panelClass: 'error-snackbar' });
  }
}
