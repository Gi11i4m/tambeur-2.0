import { Component } from '@angular/core';
import { CartService } from '../core/cart.service';
import { Order } from '../core/order.service';

@Component({
  selector: 'tb-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent {

  displayedColumns = ['name', 'amount', 'bread', 'extra', 'not', 'reference', 'club', 'remove' ];

  constructor(private cartService: CartService) {}

  get orders() {
    return this.cartService.orders;
  }

  remove(order: Order) {
    this.cartService.remove(order);
  }
}
