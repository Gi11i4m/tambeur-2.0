import { NgModule } from '@angular/core';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { CartComponent } from './cart.component';

@NgModule({
  declarations: [CartComponent],
  imports: [
    CoreModule,
    SharedModule
  ],
  exports: [
    CartComponent
  ],
  entryComponents: [CartComponent]
})
export class CartModule { }
