import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { CartModule } from './cart/cart.module';
import { CoreModule } from './core/core.module';
import { LoginModule } from './login/login.module';
import { OverviewModule } from './overview/overview.module';
import { SharedModule } from './shared/shared.module';

const routes: Routes = [{
  path: '',
  pathMatch: 'full',
  redirectTo: 'login'
}];

@NgModule({
  declarations: [AppComponent],
  imports: [
    CoreModule,
    SharedModule,
    LoginModule,
    OverviewModule,
    CartModule,
    RouterModule.forRoot(routes),
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
