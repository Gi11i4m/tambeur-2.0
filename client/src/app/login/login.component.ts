import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../core/auth.service';

@Component({
  selector: 'tb-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loginForm: FormGroup;

  constructor(formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) {
    this.loginForm = formBuilder.group({
      username: [''],
      password: ['']
    });
  }

  // TODO: error handling (write http interceptor)
  login({ username, password }) {
    this.authService
      .login(username, password)
      .subscribe(_ => this.router.navigate(['/overview']));
  }
}
