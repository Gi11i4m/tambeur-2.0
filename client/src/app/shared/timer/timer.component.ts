import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tb-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent implements OnInit {

  hours: string;
  minutes: string;
  seconds: string;

  ngOnInit() {
    setInterval(() => this.calculateRemainingTimeUntil(this.getOrderTime()), 1000);
  }

  get isAfterOrderTime() {
    return this.getNow() > this.getOrderTime();
  }

  private getNow() {
    return new Date().getTime();
  }

  private getOrderTime() {
    const currentDate = new Date();
    currentDate.setHours(10, 15, 0, 0);
    return currentDate.getTime();
  }

  calculateRemainingTimeUntil(endDate: number) {
    const startDate = new Date().getTime();

    let timeRemaining = Math.abs(Math.round((endDate - startDate) / 1000));
    timeRemaining = (timeRemaining % 86400);
    const hours = Math.abs(Math.round(timeRemaining / 3600));
    timeRemaining = (timeRemaining % 3600);
    const minutes = Math.abs(Math.round(timeRemaining / 60));
    timeRemaining = (timeRemaining % 60);
    const seconds = timeRemaining;

    this.hours = ('0' + hours).slice(-2);
    this.minutes = ('0' + minutes).slice(-2);
    this.seconds = ('0' + seconds).slice(-2);
  }
}
