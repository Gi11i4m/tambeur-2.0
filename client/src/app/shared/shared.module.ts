import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material.module';
import { TimerComponent } from './timer/timer.component';

@NgModule({
  declarations: [TimerComponent],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    MaterialModule,
    TimerComponent
  ]
})
export class SharedModule { }
