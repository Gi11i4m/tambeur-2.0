import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CartService } from '../core/cart.service';
import { Order } from '../core/order.service';
import { Product, ProductService, ProductType } from '../core/product.service';

@Component({
  selector: 'tb-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  products$: Observable<Product[]>;
  types$: Observable<ProductType[]>;

  constructor(
    private productService: ProductService,
    private cartService: CartService) {}

  ngOnInit() {
    this.products$ = this.productService.getProducts();
    this.types$ = this.products$.pipe(map(products => _.uniq(products.map(product => product.type))));
  }

  productFor(type: ProductType, products: Product[]) {
    return products.filter(product => product.type === type);
  }

  iconFor(type: ProductType) {
    return {
      [ProductType.SANDWICH]: 'fa-hotdog',
      [ProductType.TRIANGLE]: 'fa-cheese',
      [ProductType.SANDWINO]: 'fa-square',
      [ProductType.WRAP]: 'fa-drum-steelpan',
      [ProductType.BURGER]: 'fa-hamburger',
      [ProductType.HOT]: 'fa-fire',
      [ProductType.COLD]: 'fa-snowflake',
      [ProductType.DESSERT]: 'fa-cookie',
    }[type];
  }

  addToCart(order: Order) {
    console.log(order);
    this.cartService.add({ ...order, amount: Number(order.amount), club: Boolean((order as unknown as { club: boolean }).club) } as Order);
  }
}
