import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../core/auth-guard.service';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { OverviewComponent } from './overview.component';
import { OrderDetailComponent } from './product-detail/order-detail.component';

const routes: Routes = [{
  path: 'overview',
  component: OverviewComponent,
  canActivate: [AuthGuardService]
}];

@NgModule({
  declarations: [
    OverviewComponent,
    OrderDetailComponent],
  imports: [
    CoreModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class OverviewModule { }
