import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Order } from 'src/app/core/order.service';
import { Product } from 'src/app/core/product.service';

@Component({
  selector: 'tb-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent {

  @Input()
  product: Product;
  @Output()
  confirm: EventEmitter<Order>;

  breads: Bread[];

  constructor() {
    this.confirm = new EventEmitter();
    this.breads = [{
      description: 'Wit',
      value: 'wit'
    }, {
      description: 'Bruin (+€0,25)',
      value: 'bruin'
    }, {
      description: 'Wit XXL (+€0,80)',
      value: 'wit XXL'
    }, {
      description: 'Bruin XXL (+€1,05)',
      value: 'bruin XXL'
    }, {
      description: 'Falouche (+€0,25)',
      value: 'falouche'
    }, {
      description: 'Fitness (+€0,40)',
      value: 'fitness'
    }];
  }
}

interface Bread {
  description: string;
  value: string;
}
