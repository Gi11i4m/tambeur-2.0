import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {}

  get userName() {
    return sessionStorage.getItem('userName');
  }

  set userName(userName: string) {
    sessionStorage.setItem('userName', userName);
  }

  set sessionId(sessionId: string) {
    sessionStorage.setItem('sessionId', sessionId);
  }

  get sessionId() {
    return sessionStorage.getItem('sessionId');
  }

  get isAuthenticated() {
    return !!this.sessionId;
  }

  login(username: string, password: string) {
    return this.http
      .post<{ sessionId: string }>(`${environment.baseUrl}/auth`, { username, password })
      .pipe(
        tap(({ sessionId }) => this.sessionId = sessionId),
        tap(_ => this.userName = username)); // TODO: replace by actual name (also present on page)
  }
}
