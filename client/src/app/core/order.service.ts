import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Product } from './product.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) {}

  order(orders: Order[]) {
    return this.http.post<OrderResponse>(`${environment.baseUrl}/orders`, orders);
  }
}

export interface Order {
  readonly product: Product;
  readonly amount: number;
  readonly extra: string;
  readonly not: string;
  readonly reference: string;
}

export interface SandwichOrder extends Order {
  readonly bread: string;
  readonly club: boolean;
}

export interface BurgerOrder extends Order {
  readonly saus: string;
}

export interface HotOrder extends Order {
  readonly bread: string;
}

export interface ColdOrder extends Order {
  readonly large: boolean;
}

export interface OrderResponse {
  readonly success: boolean;
  readonly html: string;
}
