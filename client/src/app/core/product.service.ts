import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {}

  getProducts() {
    return this.http.get<Product[]>(`${environment.baseUrl}/products`);
  }
}

export enum ProductType {
  SANDWICH = 'Broodjes',
  TRIANGLE = 'Triangles',
  SANDWINO = 'Sandwinos',
  WRAP = 'Wraps',
  BURGER = 'Burgers',
  HOT = 'Warm',
  COLD = 'Koud',
  DESSERT = 'Desserten',
  MEETING = 'Meeting'
}

export interface Product {
  id: string;
  name: string;
  description: string;
  price: number;
  type: ProductType;
}
