import { Injectable } from '@angular/core';
import { Order } from './order.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  orders: Order[];

  constructor() {
    this.orders = [];
  }

  get numberOfProducts(): number {
    return this.orders
      .map(order => order.amount)
      .reduce((a1, a2) => a1 + a2, 0);
  }

  add(order: Order) {
    this.orders = [
      ...this.orders.filter(existingOrder => existingOrder.product.id !== order.product.id),
      order
    ].filter(orderToFilter => orderToFilter.amount >= 1 && orderToFilter.amount <= 9);
  }

  remove(order: Order) {
    this.orders = this.orders.filter(existingOrder => existingOrder !== order);
  }

  clear() {
    this.orders = [];
  }
}
