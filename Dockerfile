FROM node:8-slim as builder

WORKDIR /home

# Install Chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get update && apt-get install -y google-chrome-stable

COPY . .

WORKDIR /home/api
RUN npm install
RUN npm run test

WORKDIR /home/client
RUN npm install
RUN npm run lint
RUN npm run test
RUN npm run build -- --prod

WORKDIR /home
RUN cp -R client/dist/tambeur api/build/main/app


FROM node:8-alpine
COPY --from=builder /home/api/node_modules ./node_modules
COPY --from=builder /home/api/build/main .
EXPOSE 3000
CMD node index.js
