# Tambeur

## Install dependencies
Run `npm install` in `/api` and in `/client`.

## API
Run the API with `npm start` in `/api`.

The API is an Express app. It fetches data from the [Tambeur website](https://www.tambeurservice.be/) and maps this to a clean RESTful webservice.

## Client
Run the client with `npm start` in `/client`

The Client is an Angular app that shows a simplified and modernized version of the [Tambeur website](https://www.tambeurservice.be/).

## "Security"
This applications runs on top of the existing site, scraping its HTML and submitting form requests to it. It's as secure as the original site, which isn't very secure but secure enough for a sandwich delivery site.